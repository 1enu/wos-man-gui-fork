main.py:
	pyinstaller --windowed --noconfirm --add-data "src;src" --add-data "res;res" --add-data "LICENSE;." --name wos-manager --icon res/icons/wos-manager.ico src/main.py

win-build:
	pyinstaller --windowed --noconfirm --add-data "src;src" --add-data "res;res" --add-data "LICENSE;." --name wos-manager --icon res/icons/wos-manager.ico src/main.py
	tar.exe -a -c -f dist\wos-manager-portable.zip -C dist wos-manager
	"C:\Program Files (x86)\NSIS\makensis.exe" dist\wos-manager\res\wos-installer-build.nsi

win-build-nuitka:
	nuitka \
	--standalone \
	--disable-console \
	--remove-output \
	--enable-plugin=pyside6 \
	--include-data-files=LICENSE=LICENSE \
	--include-data-dir=res=res \
	--product-version="2.0.0" \
	--windows-icon-from-ico=res/icons/wos-manager.ico \
	--output-filename="wos-manager" \
	--output-dir=dist \
	--windows-force-stderr-spec=errors.txt \
	--user-package-configuration-file=nuitka-config.yml \
	src/main.py
	if exist dist\wos-manager\ rmdir /q /s dist\wos-manager
	ren dist\main.dist wos-manager
	tar.exe -a -c -f dist\wos-manager-portable.zip -C dist wos-manager
	"C:\Program Files (x86)\NSIS\makensis.exe" dist\wos-manager\res\wos-installer-build.nsi

clean:
	find build/ dist/ -mindepth 1 -and -not -name ".gitkeep" -and -not -name "wos-manager-portable.zip" -and -not -name "wos-manager-installer.exe" | xargs -I{{{}}} rm -rf {{{}}}

win-clean:
	forfiles /p "build" /c "cmd /c if not @file==\".gitkeep\" (if @isdir==TRUE (rmdir /q /s @file) else del /q @file)"
	forfiles /p "dist" /c "cmd /c if not @file==\".gitkeep\" if not @file==\"wos-manager-portable.zip\" if not @file==\"wos-manager-installer.exe\" (if @isdir==TRUE (rmdir /q /s @file) else del /q @file)"
