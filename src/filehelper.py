#    This file is part of wos-manager.
#
#   wos-manager is free software: you can redistribute it and/or modify it under the terms of the GNU Affero Public License
#   as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#   wos-manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU Affero Public License for more details.
#
#   You should have received a copy of the GNU Affero Public License along with wos-manager. If not, see <https://www.gnu.org/licenses/>. 

import json, sys, os, re, shutil, requests
from pathlib import Path

if getattr(sys, 'frozen', False):
    # So pyinstaller --onefile can properly find where the files are
    WorkingDirectory = Path(__file__).parent
elif __file__:
    # Two parent since the main.py is inside /src
    WorkingDirectory = Path(__file__).parent.parent

Config = re.sub("\s*#.*", "", (WorkingDirectory / "res/wos-manager.conf").read_text())
Config = json.loads(Config)

#icon paths
SettingsIconPath = WorkingDirectory/"res/icons/settings.png"
RevertIconPath = WorkingDirectory/"res/icons/revert.png"
CloseIconPath = WorkingDirectory/"res/icons/close.png"
CreateIconPath = WorkingDirectory/"res/icons/create.png"
AppIcon = WorkingDirectory/"res/icons/wos-manager.ico"
PlanetIconPath = WorkingDirectory/"res/icons/planet-without-rings.png"
RingedPlanetIconPath = WorkingDirectory/"res/icons/planet-with-rings.png"

DefaultThumbnailPath = WorkingDirectory/"res/thumbnail.png"
ModelsDirectory = WorkingDirectory/"res/models"
DefaultModelPath = WorkingDirectory/"res/default-model"
UniverseMapPath = WorkingDirectory/"res/universe-maps/universe-one.db"

# a list containing functions that will be called when all models need to be updated
UpdateCallbacks = []

# a list containing funtions that will be called with status data
InfoCallbacks = []



# this is a helper class that acts as an interface for interacting with data from models
class ModelData():
    def __init__(self, MetadataPath):
        self.MetadataPath = MetadataPath
        self.ModelDirectoryPath = MetadataPath.parent

        self.ImportMetadata()

    
    def ImportMetadata(self):
        try:
            self.Metadata = json.loads(self.MetadataPath.read_text())
        except:
            #sidebar can trigger the error, preventing updating the list.
            pass

        # {{{ getting metadata from file, and making error handlers so if some metadata is missing, it gets set to a default value
        try:
            self.ModelDataPath = self.Metadata["ModelDataPath"]
        except:
            self.ModelDataPath = "modeldata.txt"

        try:
            self.Name = self.Metadata["Name"]
        except:
            self.Name = "new model"

        try:
            self.Description = self.Metadata["Description"]
        except:
            self.Description = "example description"

        try:
            self.ThumbnailPath = self.Metadata["ThumbnailPath"]
        except:
            self.ThumbnailPath = "thumbnail"

        try:
            self.PasteLink = self.Metadata["PasteLink"]
        except:
            self.PasteLink = None

        try:
            self.RobloxPlacePath = self.Metadata["RobloxPlacePath"]
        except:
            self.RobloxPlacePath = "Place"

        try:
            self.PermamentLink = self.Metadata["PermamentLink"]
        except:
            self.PermamentLink = False
        # }}}

    def ExportMetadata(self):
        self.Metadata = {
                "ModelDataPath": self.ModelDataPath,
                "Name": self.Name,
                "Description": self.Description,
                "ThumbnailPath": self.ThumbnailPath,
                "RobloxPlacePath": self.RobloxPlacePath,
                "PermamentLink": self.PermamentLink,
                "PasteLink": self.PasteLink
                }

        self.MetadataPath.write_text(json.dumps(self.Metadata, sort_keys=True, indent=4))


    def RenewPaste(self):
        if Config["Use0x0"]: #dont renew if the user dosent want to use 0x0
            self.PermamentLink = False #this is so if you renew your link while having set a permament link, it removes the permament link status
            # Deals with file open() errors and displays to creditlabel if it errors
            try:
                TextPath = self.ModelDirectoryPath / self.ModelDataPath
                if TextPath.stat().st_size == 0:
                    ShowInfo("There is no text to be uploaded!")
                    return "There is no text to be uploaded!"
                with TextPath.open() as FileToBeUploaded:
                    files = {
                        "file": FileToBeUploaded,
                        "secret": (None, ""),
                        "expires": (None, "48"),
                    }

                    # Uses requests for easier handling of http errors
                    try:
                        response = requests.post(Config["Instance"], files=files)
                        response.raise_for_status() #errors if the url has problem

                        # Sets the paste link and removes newline after it.
                        self.PasteLink = response.text.rstrip()
                        self.ExportMetadata()
                        ShowInfo("Link renewed!")
                    except requests.exceptions.RequestException as e:
                        ShowInfo(f"{type(e)}: {e.args[0]}")
                        return f"{type(e)} {e.args[0]}"

            except IOError as e:
                ShowInfo(f"{type(e)}: {e.args[0]}")
                return f"{type(e)}: {e.args[0]}"

            

    def WriteModelData(self, NewValue):
        SameAsCurrent = self.GetModelCode() == NewValue
        if SameAsCurrent is False:
            (self.ModelDirectoryPath / self.ModelDataPath).write_text(NewValue)
            if not self.IsUsingPermamentLink():
                #Erase the old paste link so copy link creates a new paste.
                #Only happens if the value is new so pasting the same model code
                #does not upload the exact same file again
                self.PasteLink = None
                self.ExportMetadata()
                Update()

        if not self.IsUsingPermamentLink():
            #Diplays the label even if the code is the same
            ShowInfo("Model code set!")
        else:
            #Notify model code is in permanent link mode
            ShowInfo("Local model code set! (Using permanent link)")


    def IsUsingPermamentLink(self):
        #if using a permament link or not using 0x0, it treats it as a permament link
        return self.PermamentLink or not Config["Use0x0"]
    
    def GetLink(self):
        if not self.IsUsingPermamentLink() and not self.IsPasteValid():
            ErrorText = self.RenewPaste()
            if ErrorText:
                ShowInfo(ErrorText)
                return self.PasteLink
        ShowInfo("Link copied!")
        return self.PasteLink
        
    def IsPasteValid(self):
        if self.PasteLink:
            try:
                OKStatus = requests.head(self.PasteLink).ok
                return OKStatus
            except:
                return False

    def GetModelCode(self):
        ShowInfo("Got model code")
        return (self.ModelDirectoryPath/self.ModelDataPath).read_text()

    def GetThumbnailPath(self):
        if (self.ModelDirectoryPath/self.ThumbnailPath).is_file():
            return self.ModelDirectoryPath / self.ThumbnailPath
        else:
            return DefaultThumbnailPath

    # method that sets the thumbnail of the model
    def SetThumbnail(self, NewPath):
        NewPath = Path(NewPath)
        if NewPath.is_file:
            self.ThumbnailPath = str(NewPath)
            self.ExportMetadata() 
            Update()

    # method that sets the roblox place where the model is located
    def SetRobloxPath(self, NewPath):
        NewPath = Path(NewPath)
        if NewPath.is_file:
            self.RobloxPlacePath = str(NewPath)
            self.ExportMetadata()
            ShowInfo("Roblox place set!")

    # method that opens the roblox place where the model is located
    def OpenRobloxPlace(self):
        if self.HasSetRobloxPlace():
            if sys.platform.startswith("linux"):
                os.system("setsid -f xdg-open {}".format(self.ModelDirectoryPath/self.RobloxPlacePath))
            else:
                os.system("start \"\" \"{}\"".format(self.ModelDirectoryPath/self.RobloxPlacePath))
        else:
            ShowInfo("Trying to open a non existent roblox place.")

    def HasSetRobloxPlace(self):
        return (self.ModelDirectoryPath/self.RobloxPlacePath).is_file()

    # method that sets the model name
    def SetName(self, NewName):
        self.Name = NewName
        self.ExportMetadata()
        Update()

    # sets the permament link, if the new permament link is empty, removes the permament link status
    def SetPermamentLink(self, NewLink):
        if NewLink:
            self.PermamentLink = True
            self.PasteLink = NewLink
            ShowInfo("Permament link set!")
        else:
            self.PermamentLink = False
            self.PasteLink = None
            ShowInfo("Permament link removed!")

        self.ExportMetadata()
        # This is needed to update the link of the slim mode
        # after updating the link on the sidebar
        Update()

    # sets the description of the model
    def SetDescription(self, NewDescription):
        self.Description = NewDescription
        self.ExportMetadata()
        Update()

    # removes the model data and its files, also redraws model data widgets
    def Delete(self):
        try:
            shutil.rmtree(self.ModelDirectoryPath)
        except:
            #sidebar can trigger the error, preventing updating the list.
            pass
        finally:
            Update()

# a function that returns all models
def GetModels():
    Models = []
    for i in ModelsDirectory.iterdir():
        Models.append(ModelData(i / "metadata.json"))
    return Models

# procedure to update all widgets displaying model data to redraw
def Update():
    for i in UpdateCallbacks:
        i()

# a function that adds callback functions that should be called when redrawing everything
def AddToUpdateCallback(Callback):
    UpdateCallbacks.append(Callback)

#function to tell the callbacks to display info
def ShowInfo(Info):
    for i in InfoCallbacks:
        i(Info)

# function to add callback functions that will display info
def AddToInfoCallback(Callback):
    InfoCallbacks.append(Callback)

# creates a new model
def CreateNewModel():
    Max = 0
    for i in ModelsDirectory.iterdir():
        if Max < int(i.name):
            Max = int(i.name)
    NewModel = shutil.copytree(DefaultModelPath, WorkingDirectory/"res/models/{}".format(Max+1))
    Update()
