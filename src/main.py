#    This file is part of wos-manager.
#
#   wos-manager is free software: you can redistribute it and/or modify it under the terms of the GNU Affero Public License
#   as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#   wos-manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU Affero Public License for more details.
#
#   You should have received a copy of the GNU Affero Public License along with wos-manager. If not, see <https://www.gnu.org/licenses/>. 
import sys, ModelPage, filehelper as FileHelper, WosGuide
from PySide6.QtWidgets import QApplication, QVBoxLayout, QMainWindow, QWidget, QTabWidget, QLabel, QSizePolicy
from PySide6.QtCore import Qt, QTimer
from PySide6.QtGui import QColor, QIcon, QPalette

##{{{ Window: the main window
class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("WOS manager")

        self.WindowContent = WindowContent()

        self.setCentralWidget(self.WindowContent)
#}}}

#{{{ WindowContent: widget containing all the things in the window
class WindowContent(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)

        FileHelper.AddToInfoCallback(self.SetCreditLabelText)

        self.TabWidget = QTabWidget()
        self.TabWidget.currentChanged.connect(self.TabChanged)

        self.ModelWidget = ModelPage.MainWidget()
        self.WosGuideWidget = WosGuide.MainWidget()

        self.TabWidget.addTab(self.ModelWidget, "Models")
        self.TabWidget.addTab(self.WosGuideWidget, "Wos guide")

        #Setup creditlabel and timer
        self.DefaultCreditLabelText = "v2.0.0: made by Adolf freeman and EnU"
        self.CreditLabel = QLabel(self.DefaultCreditLabelText)
        FileHelper.CreditLabel = self.CreditLabel

        self.CreditTimer = QTimer()
        self.CreditTimer.setSingleShot(True)
        self.CreditTimer.timeout.connect(self.SetCreditLabelToDefault)

        self.TabChanged(0)
        self.Layout.addWidget(self.TabWidget)
        self.Layout.addWidget(self.CreditLabel)

    def TabChanged(self, Index):
        for i in range(self.TabWidget.count()):
            if i != Index:
                self.TabWidget.widget(i).setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
            else:
                self.TabWidget.widget(i).setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)

    def SetCreditLabelText(self, NewValue):
        self.CreditLabel.setText(NewValue)
        self.CreditTimer.start(3*1000)

    def SetCreditLabelToDefault(self):
        self.CreditLabel.setText(self.DefaultCreditLabelText)
#}}}


app = QApplication(sys.argv)
app.setWindowIcon(QIcon(str(FileHelper.AppIcon)))
app.setStyle("Fusion")

# Now use a palette to switch to dark colors:
palette = QPalette()
palette.setColor(QPalette.Window, QColor(53, 53, 53))
palette.setColor(QPalette.WindowText, Qt.white)
palette.setColor(QPalette.Base, QColor(25, 25, 25))
palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
palette.setColor(QPalette.ToolTipBase, Qt.black)
palette.setColor(QPalette.ToolTipText, Qt.white)
palette.setColor(QPalette.Text, Qt.white)
palette.setColor(QPalette.Button, QColor(53, 53, 53))
palette.setColor(QPalette.ButtonText, Qt.white)
palette.setColor(QPalette.BrightText, Qt.red)
palette.setColor(QPalette.Link, QColor(42, 130, 218))
palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
palette.setColor(QPalette.HighlightedText, Qt.black)
app.setPalette(palette)


MainWindow = Window()
MainWindow.show()
sys.exit(app.exec())
