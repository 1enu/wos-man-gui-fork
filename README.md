# wos-manager
This is a qt app for managing models for the hit game waste of space, it centeralizes all your models in one place,\
Allows you to easily get paste links that automatically renew and other features for managing models

# Overview
the app has follwoing tabs:

## Model
![imgae](res/icons/modelpage.png)
This is a tab that has a list of models and a searchbar. It acts as an interface to interact with models.\
You can click on any of the model widgets and select them, this will show up a sidebar that allows you to modify the model.\
You can also modify the model data with the gear button on each model widget.

the gear button has the following options to interact with the model:
- Copy link: copies the link of the paste to clipboard (and also renews the link if it expired)
- Copy model code: copies the model code
- Set model code: show a dialog where you paste in the new model code
- Set roblox place: sets up the roblox place where the model is located so it casn be opened from the app
- Set permament link: sets a permament link and disables automatic link renowal, this link can be from any paste site, not just 0x0, to disable automatic links, click on the "renew link" option
- Open roblox place: opens up the roblox place where the model is located (if you set the roblox place)
- Set thumbnail: sets the thumbnail of the model
- Delete: deletes the model (duh)
- Renew Link: forces the app to renew the link, in normal usage this is never needed

The sidebar can edit extra data, like the name and the description

## Slim
![image](res/icons/slimmodelpage.png)\
This is a simple list of all models and a searchbar, you can click on the name of the model to copy its link.\
This is a mode you can use if you dont want the app to take up much screen space

# Installation

you can install the app [here](https://gitlab.com/vim_disel/wos-manager/-/releases/1.1.0)

# Building
you will need the following dependencies:
- python-rapidfuzz
- python-requests
- python-pyside6
- pyinstaller or nuitka
- make (you can easily do without it though)

and just run ```make``` in the project directory, if you're using windows then you will need to run ```make win-build``` instead, you will need to install NSIS though

# Notes

## using a different 0x0 instance
You can change the 0x0 instance in the config file, the config file name is "wos-manager.conf" and should be in the same place as your model files,\
when setting your own instance, make sure to prepend the domain with the protocol you want to use to ensure you always use a secure protocol like https\
the links will be changed the next time they need to be renewed, so if you want the changes to be applied immediately then you will have to go through every model and renew all the links

## not using 0x0 at all
In the config file you have the option to disable any connections to 0x0, this means your models wont be hosted on that site.\
Doing so does remove a lot of features though, mainly automatic links, instead you will have to set a permament link for every model you make\
after disabling 0x0, all old 0x0 links are still saved so you can use them untill they expire, but you should probably replace them with permament ones

## building using nuitka(windows)
**NOTE:** The current dist files(portable and installer) uses nuitka as of **1.1.0** as an attempt to reduce anti-virus(mainly windows defender) false positive reports. To build using nuitka, run `make win-build-nuitka` instead.

If you are using nuitka **1.7.10 or below**, do the following:
> **NOTE:** A [pull request](https://github.com/Nuitka/Nuitka/pull/2408) has already added this to nuitka, so future versions after 1.7.10 might not need to do this fix.

1. Go to where nuitka is installed by python
2. Open the file `nuitka\plugins\standard\standard.nuitka-package.config.yml` using a text editor(notepad, etc.)
3. Use CTRL+F and search for "rapidfuzz.distance" and should see this:
```yml
- module-name: 'rapidfuzz.distance'
  implicit-imports:
    - depends:
        - '._initialize_py'
        - '._initialize_cpp'
        - '.Hamming_py'
        ... (more modules)
```
4. Add the following code at the end of rapidfuzz.distance `depends` list if its missing:
```yml
        - '.metrics_cpp'
        - '.metrics_py'
```

# Project Status
I consider this project to be feature complete and i dont see any reason to make any new updates, this means there wont be any more features added
